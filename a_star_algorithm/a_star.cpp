#include "a_star.h"

namespace AStarAlgorithm {

	const char* HeuristicToString(HeuristicType heuristic) {
		switch (heuristic) {
			case kManhattan:
				return "Manhattan";
			case kDiagonal:
				return "Diagonal";
			case kEuclidean:
				return "Euclidean";
			case kDijkstra:
				return "Dijkstra";
			default:
				return "";
		}
		return "";
	}
	int Distance(Node node, Node goal) {
		float dx = (float)abs(goal.x_ - node.x_);
		float dy = (float)abs(goal.y_ - node.y_);
		dx = dx*dx;
		dy = dy*dy;
		return  (int)(10 * sqrt(dx + dy));
	}

	int Heuristic(Node node, Node goal, HeuristicType heuristic_type) {
		int D = 10;
		int D2 = 14;
		switch (heuristic_type) {
			case kManhattan:
			{
				return (int)(D * ((abs(node.x_ - goal.x_) + abs(node.y_ - goal.y_))));
			}
			case kDiagonal:
			{
				float dx = (float)abs(node.x_ - goal.x_);
				float dy = (float)abs(node.y_ - goal.y_);
				return (int)(D * (dx + dy) + (D2 - 2 * D) * std::min(dx, dy));
			}
			case kEuclidean:
			{
				float dx = (float)(node.x_ - goal.x_);
				float dy = (float)(node.y_ - goal.y_);
				dx = dx * dx;
				dy = dy * dy;
				return (int)(D * sqrt(dx + dy));
			}
			case kDijkstra:
				return 0;
			default:
				return 0;
		}

	}

	bool node_comp(const Node *lhs, const Node *rhs) {
		//return lhs->f_cost_ < rhs->f_cost_;
		// tie breaker sorting other method, decided not to use this one
		if (lhs->f_cost_ == rhs->f_cost_) {
			return lhs->h_cost_ < rhs->h_cost_;
		}
		return lhs->f_cost_ < rhs->f_cost_;
	}
	bool AStarRoute(Node* start, Node* end, HeuristicType heuristic_type) {
		if (start == nullptr || end == nullptr) {
			return false;
		}

		std::set<Node* > open;
		std::set<Node* > closed;

		//>>> FIND PATH <<<	
		start->g_cost_ = 0;
		start->h_cost_ = (float)Heuristic(*start, *end, heuristic_type);
		start->f_cost_ = start->g_cost_ + start->h_cost_;
		// add start to open set
		open.insert(start);
		Node *current;
		while (!open.empty()) {
			// pick node in open with lowest f cost		
			current = *std::min_element(open.begin(), open.end(), node_comp);;
			if (current == end) {
				return true;
			}
			open.erase(std::min_element(open.begin(), open.end(), node_comp));
			closed.insert(current);
			for (std::list<Node* >::iterator node = current->neighbours_.begin(); node != current->neighbours_.end(); node++) {

				// if node in closed			
				if (closed.find(*node) != closed.end()) {
					continue;	// then its already done so skip it
				}
				float new_g = current->g_cost_ + Distance(*current, *(*node));
				// add node if its not in open
				if (!open.insert(*node).second) {
					if (new_g > (*node)->g_cost_) {
						// already has faster route so skip it
						continue;
					}
				}
				(*node)->g_cost_ = new_g;
				(*node)->h_cost_ = (float)Heuristic(*(*node), *end, heuristic_type);
				(*node)->f_cost_ = (*node)->g_cost_ + (*node)->h_cost_;
				(*node)->parent_ = current;
			}
		}
		return false;
	}

	bool AStarRouteUnorderedSet(Node* start, Node* end, HeuristicType heuristic_type) {
		if (start == nullptr || end == nullptr) {
			return false;
		}
		std::unordered_set<Node* > open;
		std::unordered_set<Node* > closed;

		//>>> FIND PATH <<<	
		start->g_cost_ = 0;
		start->h_cost_ = (float)Heuristic(*start, *end, heuristic_type);
		start->f_cost_ = start->g_cost_ + start->h_cost_;
		// add start to open set
		open.insert(start);
		Node *current;
		while (!open.empty()) {
			// pick node in open with lowest f cost		
			current = *std::min_element(open.begin(), open.end(), node_comp);;
			if (current == end) {
				return true;
			}
			open.erase(std::min_element(open.begin(), open.end(), node_comp));
			closed.insert(current);
			for (std::list<Node* >::iterator node = current->neighbours_.begin(); node != current->neighbours_.end(); node++) {

				// if node in closed			
				if (closed.find(*node) != closed.end()) {
					continue;	// then its already done so skip it
				}
				float new_g = current->g_cost_ + Distance(*current, *(*node));
				// add node if its not in open
				if (!open.insert(*node).second) {
					if (new_g > (*node)->g_cost_) {
						// already has faster route so skip it
						continue;
					}
				}
				(*node)->g_cost_ = new_g;
				(*node)->h_cost_ = (float)Heuristic(*(*node), *end, heuristic_type);
				(*node)->f_cost_ = (*node)->g_cost_ + (*node)->h_cost_;
				(*node)->parent_ = current;
			}
		}
		return false;
	}

	std::list<Node> GeneratePath(Node* start, Node* end) {
		std::list<Node> path;
		if (start == nullptr || end == nullptr) {
			return path;
		}
		Node *node = end;
		while (node != start) {
			if (node->parent_ == nullptr) {
				break;
			}
			path.push_back(*node);
			node = node->parent_;
		}
		path.push_back(*start);
		path.reverse();
		return path;
	}

	Node* GetNode(int x, int y, std::list<Node> &nodes) {
		Node *node_;
		for (std::list<Node>::iterator current = nodes.begin(); current != nodes.end(); current++) {
			if ((current->x_ == x) && (current->y_ == y)) {
				return node_ = &(*current);
			}
		}
		//std::cout << "No Valid Node at this position" << std::endl;
		return node_ = nullptr;
	}

}