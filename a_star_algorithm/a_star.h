#ifndef _A_STAR_H
#define _A_STAR_H

#include <set>			// For A* open/closed sets.
#include <unordered_set>
#include <iostream>		// For cout etc.
#include <algorithm>	// For min_element, finds lowest value element.
#include <vector>
#include "node.h"

namespace AStarAlgorithm {
	enum HeuristicType {
		kManhattan,
		kDiagonal,
		kEuclidean,
		kDijkstra
	};

	const char* HeuristicToString(HeuristicType heuristic);

	/// <summary>
	/// Measured distance between 2 nodes.
	/// </summary>
	/// <param name="node"> Initial node. </param>
	/// <param name="goal"> Destination Node. </param>
	/// <returns> The distance between the 2 nodes. </returns>
	int Distance(Node node, Node goal);

	/// <summary>
	/// Estimation of distance to goal, best to modify this value to increase performance.
	/// </summary>
	/// <param name="node"> Initial node. </param>
	/// <param name="goal"> Destination Node. </param>
	/// <param name="heuristic_type"> The type of heuristic calculation. </param>
	/// <returns> The distance between the 2 nodes. </returns>
	int Heuristic(Node node, Node goal, HeuristicType heuristic_type);

	/// <summary>
	/// Used to find the lowest node by f_cost in a stl container.
	/// </summary>
	bool node_comp(const Node *lhs, const Node *rhs);

	/// <summary>
	/// Populates the parent nodes using the A* algorithm.
	/// Returns true if successful, false if it failed with invalid points or no possible path.
	/// Call <see cref="GeneratePath"/> to retrieve the generated path.
	/// </summary>
	/// <param name="start"> Starting point. </param>
	/// <param name="end"> Destination Node. </param>
	/// <param name="heuristic_type"> The type of heuristic calculation. </param>
	/// <returns> True if path found. </returns>
	bool AStarRoute(Node* start, Node* end, HeuristicType heuristic_type);

	bool AStarRouteUnorderedSet(Node* start, Node* end, HeuristicType heuristic_type);

	/// <summary>
	/// Retrieves a path between the 2 nodes using the parents and back tracking.
	/// If no valid path will return garbage path of closest route, maybe.
	/// </summary>
	/// <param name="start"> Starting point. </param>
	/// <param name="end"> Destination Node. </param>
	/// <returns> List of all nodes to reach the destination. </returns>
	std::list<Node> GeneratePath(Node* start, Node* end);

	/// <summary>
	/// Get a node from a list of nodes.
	/// </summary>
	/// <param name="x"> X value of node to match.</param>
	/// <param name="y"> Y value of node to match.</param>
	/// <param name="nodes"> List of nodes to traverse.</param>
	/// <returns> Pointer to the node, or nullptr if not found.</returns>
	Node* GetNode(int x, int y, std::list<Node> &nodes);

}
#endif

