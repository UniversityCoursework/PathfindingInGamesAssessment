#ifndef _NODE_H
#define _NODE_H

#include <list>

namespace AStarAlgorithm {
	/// <summary>
	/// Stores x/y and f/g/h, Initialised to -1. parent is set to nullptr.
	/// </summary>
	class Node {
	public:
		Node() :g_cost_(-1), h_cost_(-1), f_cost_(-1) { parent_ = nullptr; };
		Node(int _x, int _y) :x_(_x), y_(_y), g_cost_(-1), h_cost_(-1), f_cost_(-1) { parent_ = nullptr; };
		Node(int _x, int _y, float move_cost) :x_(_x), y_(_y), g_cost_(-1), h_cost_(-1), f_cost_(-1), move_cost_(move_cost) { parent_ = nullptr; };
		int x_, y_;
		float g_cost_, h_cost_, f_cost_;

		float move_cost_; // not implemented/used

		Node *parent_;

		std::list<Node* > neighbours_;

		/// <summary>
		/// Connects node to other node. Works only in one direction. Adds to only its neighbours list.
		/// </summary>
		/// <param name="neighbour"> The node to connect. </param>
		void Connect(Node *neighbour) {
			neighbours_.push_back(neighbour);
		}

		/// <summary>
		/// Connects node to other node. Connects in both directions. Adds the node to its neigbour list, and itself to the nodes neighbour list.
		/// </summary>
		/// <param name="neighbour"> The node to connect. </param>
		void Join(Node *neighbour) {
			neighbours_.push_back(neighbour);
			neighbour->neighbours_.push_back(this);
		}
	};
}

#endif
