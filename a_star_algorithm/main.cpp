#include <list>

#include <chrono>
#include <fstream>

#include "a_star.h"

using std::chrono::duration_cast;
using std::chrono::nanoseconds;
using AStarAlgorithm::Node;

// Define the alias "the_clock" for the clock type.
typedef std::chrono::high_resolution_clock the_clock;

/// <summary> 
/// Generate a list with nodes linked to neighbours based on a grid. -1 node, -2 blocked.
/// </summary>
/// <param name="has_diagonals"> Cannot use manhattan distance if it has diagonals.</param>
template <int height, int width>
std::list<Node> CreateNodes(int(&grid)[height][width], bool has_diagonals = false) {
	std::list<Node> nodes;
	// Convert grid into list of nodes with x and y values.
	for (int j = 0; j < height; j++) {
		for (int i = 0; i < width; i++) {
			/// <summary> check is usable </param>
			if (grid[j][i] == -1) {
				nodes.push_back(Node(i, j));
			}
		}
	}
	// Generate neighbours	
	for (std::list<Node>::iterator current = nodes.begin(); current != nodes.end(); current++) {
		for (std::list<Node>::iterator node = nodes.begin(); node != nodes.end(); node++) {
			// N
			if ((node->x_ == (current->x_)) && (node->y_ == (current->y_ - 1))) {
				current->Connect(&(*node));
				continue;
			}
			// S
			if ((node->x_ == (current->x_)) && (node->y_ == (current->y_ + 1))) {
				current->Connect(&(*node));
				continue;
			}
			// W
			if ((node->x_ == (current->x_ - 1)) && (node->y_ == (current->y_))) {
				current->Connect(&(*node));
				continue;
			}
			// E
			if ((node->x_ == (current->x_ + 1)) && (node->y_ == (current->y_))) {
				current->Connect(&(*node));
				continue;
			}
			if (has_diagonals) {
				// NE
				if ((node->x_ == (current->x_ + 1)) && (node->y_ == (current->y_ + 1))) {
					current->Connect(&(*node));
					continue;
				}
				// NW
				if ((node->x_ == (current->x_ - 1)) && (node->y_ == (current->y_ + 1))) {
					current->Connect(&(*node));
					continue;
				}
				// SE
				if ((node->x_ == (current->x_ + 1)) && (node->y_ == (current->y_ - 1))) {
					current->Connect(&(*node));
					continue;
				}
				// SW
				if ((node->x_ == (current->x_ - 1)) && (node->y_ == (current->y_ - 1))) {
					current->Connect(&(*node));
					continue;
				}
			}


		}
	}
	return nodes;
}

/// <summary> 
/// Shows grid in recognisable format.
/// </summary>
template <int height, int width>
void DisplayGrid(int(&grid)[height][width]) {
	std::cout << std::endl;
	for (int j = 0; j < height; j++) {
		for (int i = 0; i < width; i++) {
			printf("%4d", grid[j][i]);
		}
		std::cout << std::endl;
	}
}

/// <summary> 
/// Makes the grid look pretty. 
/// </summary>
template <int height, int width>
void DisplayPrettyGrid(int(&grid)[height][width]) {
	std::cout << std::endl;
	for (int j = 0; j < height; j++) {
		for (int i = 0; i < width; i++) {
			if (grid[j][i] == -1) {
				printf("%c%c", 176, 176);
			} else if (grid[j][i] == -2) {
				printf("  ");
			} else if (grid[j][i] == 0) {
				printf("%c%c", 177, 177);
			}
		}
		std::cout << std::endl;
	}
}

/// <summary> 
/// Overlays the path onto the grid and shows it in a pretty pattern. 
/// Modifies the grid, displays, then reverts the changes. 
/// </summary>
template <int height, int width>
void OverlayPathToGrid(int(&grid)[height][width], std::list<Node> path) {
	// Overrides each path point to zero
	for (std::list<Node>::iterator current = path.begin(); current != path.end(); current++) {
		grid[current->y_][current->x_] = 0;
	}
	DisplayPrettyGrid(grid);
	// Reverts the changes to the grid.
	for (std::list<Node>::iterator current = path.begin(); current != path.end(); current++) {
		grid[current->y_][current->x_] = -1;
	}
}

/// <summary> 
/// similar to overlay path, shows f/g/h costs in place of grid. 
/// </summary>
template <int height, int width>
void OverlayCosts(int(&grid)[height][width], std::list<Node> nodes) {
	std::cout << std::endl << "f_cost_";
	for (std::list<Node>::iterator current = nodes.begin(); current != nodes.end(); current++) {
		grid[current->y_][current->x_] = (int)current->f_cost_;
	}
	DisplayGrid(grid);

	std::cout << std::endl << "g_cost_";
	for (std::list<Node>::iterator current = nodes.begin(); current != nodes.end(); current++) {
		grid[current->y_][current->x_] = (int)current->g_cost_;
	}
	DisplayGrid(grid);

	std::cout << std::endl << "h_cost_";
	for (std::list<Node>::iterator current = nodes.begin(); current != nodes.end(); current++) {
		grid[current->y_][current->x_] = (int)current->h_cost_;
	}
	DisplayGrid(grid);
	// Reverts the changes to the grid.
	for (std::list<Node>::iterator current = nodes.begin(); current != nodes.end(); current++) {
		grid[current->y_][current->x_] = -1;
	}
}
/// <summary> 
/// Cleans out all the nodes value in preperation for another test. 
/// </summary>
void ResetNodes(std::list<Node> &nodes) {
	for (std::list<Node>::iterator current = nodes.begin(); current != nodes.end(); current++) {
		current->f_cost_ = -1;
		current->g_cost_ = -1;
		current->h_cost_ = -1;
		current->parent_ = NULL;
	}
}

/// <summary> 
/// Generates a path between 2 given points.
/// Wraps up the code. and produces f/g/h costs and time. runs the test 1000 times. 
/// </summary>
template <int height, int width>
void DebugTestPath(int(&grid)[height][width], int s_x, int s_y, int e_x, int e_y, AStarAlgorithm::HeuristicType heuristic = AStarAlgorithm::kManhattan, bool has_diagonals = false) {
	std::list<Node> nodes;
	std::list<Node> path;
	if (has_diagonals) {
		nodes = CreateNodes(grid, true);
		std::cout << "Has diagonals" << std::endl;
	} else {
		nodes = CreateNodes(grid);
	}
	const int timing_increase_amount = 1000;
	the_clock::time_point start = the_clock::now();
	for (int i = 0; i < timing_increase_amount; i++) {
		ResetNodes(nodes);
		path.clear();

		Node *start_node = AStarAlgorithm::GetNode(s_x, s_y, nodes);
		Node *end_node = AStarAlgorithm::GetNode(e_x, e_y, nodes);
		// Line to actually test for AStar
		if (AStarAlgorithm::AStarRoute(start_node, end_node, heuristic)) {
			path = AStarAlgorithm::GeneratePath(start_node, end_node);
		}
	}
	the_clock::time_point end = the_clock::now();
	auto time_taken = duration_cast<nanoseconds>(end - start).count();
	std::cout << "Test took " << time_taken / timing_increase_amount << " ns. " << std::endl;
	// allows you to see what nodes where actually tested
	std::cout << "Heuristic: " << AStarAlgorithm::HeuristicToString(heuristic) << std::endl;
	std::cout << "Start: X:" << s_x << "Y:" << s_y << "\t";
	std::cout << "End: X:" << e_x << "Y:" << e_y << std::endl;
	OverlayPathToGrid(grid, path);
	OverlayCosts(grid, nodes);
	std::cin.ignore();
}

/// <summary> 
/// Messy big test of connection between all points on grid.
/// Shows that it generates the correct path between all possible points.
/// Also diplays f/g/h costs for each node.
/// </summary>
template <int height, int width>
void DebugTestAllNodePaths(int(&grid)[height][width], AStarAlgorithm::HeuristicType heuristic = AStarAlgorithm::kManhattan, bool has_diagonals = false) {
	DisplayPrettyGrid(grid);
	std::cin.ignore();
	std::list<Node> nodes;
	std::list<Node> path;
	if (has_diagonals) {
		nodes = CreateNodes(grid, true);
	} else {
		nodes = CreateNodes(grid);
	}
	for (int s_x = 0; s_x < width; s_x++) {
		for (int s_y = 0; s_y < height; s_y++) {
			for (int e_x = 0; e_x < width; e_x++) {
				for (int e_y = 0; e_y < height; e_y++) {
					ResetNodes(nodes);
					path.clear();
					Node *start_node = AStarAlgorithm::GetNode(s_x, s_y, nodes);
					while (start_node == nullptr) {
						s_y++;
						if (s_y > height) {
							s_y = 0;
							s_x++;
						}
						start_node = AStarAlgorithm::GetNode(s_x, s_y, nodes);
					}
					Node *end_node = AStarAlgorithm::GetNode(e_x, e_y, nodes);
					while (end_node == nullptr) {
						e_y++;
						if (e_y > height) {
							e_y = 0;
							e_x++;
						}
						end_node = AStarAlgorithm::GetNode(e_x, e_y, nodes);
					}
					// Line to actually test for AStar
					if (AStarAlgorithm::AStarRoute(start_node, end_node, heuristic)) {
						path = AStarAlgorithm::GeneratePath(start_node, end_node);
					}
					// allows you to see what nodes where actually tested	
					std::cout << "Heuristic: " << AStarAlgorithm::HeuristicToString(heuristic) << std::endl;
					std::cout << "Start: X:" << s_x << "Y:" << s_y << "\t";
					std::cout << "End: X:" << e_x << "Y:" << e_y << std::endl;
					OverlayPathToGrid(grid, path);
					OverlayCosts(grid, nodes);
					std::cin.ignore();
				}
			}
		}
	}

}

/// <summary> 
/// Outputs timings to file, usefull for R statistics stuff. 
/// Runs test in dijkstra, and A* with manhattan heuristic, or kDiagonal if has diagonals true. 
/// </summary>
template <int height, int width>
void AlgorithmComparision(int(&grid)[height][width], int number_of_test, int start_x, int start_y, int end_x, int end_y, std::string filename_to_save, bool has_diagonals = false) {
	std::ofstream debug_file;
	std::list<Node> nodes;
	if (has_diagonals) {
		std::cout << filename_to_save.c_str() << "hasDiagonals" << std::endl;
		nodes = CreateNodes(grid, true);
		debug_file.open("AStar_vs_Dijkstra" + filename_to_save + "_hasDiagonals.dat");
	} else {
		std::cout << filename_to_save.c_str() << std::endl;
		nodes = CreateNodes(grid);
		debug_file.open("AStar_vs_Dijkstra" + filename_to_save + ".dat");
	}

	Node *start_node = AStarAlgorithm::GetNode(start_x, start_y, nodes);
	Node *end_node = AStarAlgorithm::GetNode(end_x, end_y, nodes);
	std::list<Node> path;
	const int timing_increase_amount = 1000;

	for (int i = 0; i < number_of_test; i++) {
		debug_file << i;

		// A* fastest first
		the_clock::time_point start = the_clock::now();
		for (int i = 0; i < timing_increase_amount; i++) {
			ResetNodes(nodes);
			path.clear();
			// Line to actually test for AStar
			if (has_diagonals) {
				if (AStarAlgorithm::AStarRoute(start_node, end_node, AStarAlgorithm::kDiagonal)) {
					path = AStarAlgorithm::GeneratePath(start_node, end_node);
				}
			} else {
				if (AStarAlgorithm::AStarRoute(start_node, end_node, AStarAlgorithm::kManhattan)) {
					path = AStarAlgorithm::GeneratePath(start_node, end_node);
				}
			}

		}
		the_clock::time_point end = the_clock::now();
		auto time_taken = duration_cast<nanoseconds>(end - start).count();
		std::cout << "Test 1 took " << time_taken / timing_increase_amount << " ns. ";
		debug_file << ", " << time_taken / timing_increase_amount;

		// slower option
		start = the_clock::now();
		for (int i = 0; i < timing_increase_amount; i++) {
			ResetNodes(nodes);
			path.clear();
			if (AStarAlgorithm::AStarRoute(start_node, end_node, AStarAlgorithm::kDijkstra)) {
				path = AStarAlgorithm::GeneratePath(start_node, end_node);
			}
		}
		end = the_clock::now();

		time_taken = duration_cast<nanoseconds>(end - start).count();
		std::cout << "Test 2 took " << time_taken / timing_increase_amount << " ns.";
		debug_file << ", " << time_taken / timing_increase_amount;

		std::cout << std::endl;
		debug_file << std::endl;
	}
	debug_file.close();

}

/// <summary> 
/// Outputs timings to file, usefull for R statistics stuff. 
/// Runs test on sets and unordered sets. 
/// </summary>
template <int height, int width>
void CompareSets(int(&grid)[height][width], int number_of_test, int start_x, int start_y, int end_x, int end_y, std::string filename_to_save) {
	std::ofstream debug_file;
	std::list<Node> nodes;
	nodes = CreateNodes(grid);
	std::cout << filename_to_save.c_str() << std::endl;
	debug_file.open("set_vs_unordered_set_" + filename_to_save + ".dat");

	Node *start_node = AStarAlgorithm::GetNode(start_x, start_y, nodes);
	Node *end_node = AStarAlgorithm::GetNode(end_x, end_y, nodes);
	std::list<Node> path;
	const int timing_increase_amount = 1000;

	for (int i = 0; i < number_of_test; i++) {
		debug_file << i;

		// Set
		the_clock::time_point start = the_clock::now();
		for (int i = 0; i < timing_increase_amount; i++) {
			ResetNodes(nodes);
			path.clear();
			if (AStarAlgorithm::AStarRoute(start_node, end_node, AStarAlgorithm::kManhattan)) {
				path = AStarAlgorithm::GeneratePath(start_node, end_node);
			}
		}
		the_clock::time_point end = the_clock::now();
		auto time_taken = duration_cast<nanoseconds>(end - start).count();
		std::cout << "Test 1 took " << time_taken / timing_increase_amount << " ns. ";
		debug_file << ", " << time_taken / timing_increase_amount;

		// UnorderedSet
		start = the_clock::now();
		for (int i = 0; i < timing_increase_amount; i++) {
			ResetNodes(nodes);
			path.clear();
			if (AStarAlgorithm::AStarRouteUnorderedSet(start_node, end_node, AStarAlgorithm::kManhattan)) {
				path = AStarAlgorithm::GeneratePath(start_node, end_node);
			}
		}
		end = the_clock::now();
		time_taken = duration_cast<nanoseconds>(end - start).count();
		std::cout << "Test 2 took " << time_taken / timing_increase_amount << " ns. ";
		debug_file << ", " << time_taken / timing_increase_amount;

		std::cout << std::endl;
		debug_file << std::endl;
	}
	debug_file.close();
}

void main() {

	// note: is [y][x] not [x][y] this was just to make layout and bug fixing easier
	// also allows for easier visualisation of whats happening
	int grid4by4[4][4] = {
		{ -1, -1, -1, -1 },
		{ -1, -1, -1, -1 },
		{ -1, -1, -1, -1 },
		{ -1, -1, -1, -1 }
	};

	int grid8by4[4][8] = {
		{ -1, -1, -1, -1, -1, -1, -1, -1 },
		{ -1, -1, -1, -1, -1, -1, -1, -1 },
		{ -1, -1, -1, -1, -1, -1, -1, -1 },
		{ -1, -1, -1, -1, -1, -1, -1, -1 }
	};

	int grid8by8[8][8] = {
		{ -1, -1, -1, -1, -1, -1, -1, -1 },
		{ -1, -1, -1, -1, -1, -1, -1, -1 },
		{ -1, -1, -1, -1, -1, -1, -1, -1 },
		{ -1, -1, -1, -1, -1, -1, -1, -1 },
		{ -1, -1, -1, -1, -1, -1, -1, -1 },
		{ -1, -1, -1, -1, -1, -1, -1, -1 },
		{ -1, -1, -1, -1, -1, -1, -1, -1 },
		{ -1, -1, -1, -1, -1, -1, -1, -1 }
	};

	int grid16by16[16][16] = {
		{ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
		{ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
		{ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
		{ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
		{ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
		{ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
		{ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
		{ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
		{ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
		{ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
		{ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
		{ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
		{ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
		{ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
		{ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
		{ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 }
	};

	int grid32by32[32][32] = {
		{ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ,-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
		{ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ,-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
		{ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ,-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
		{ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ,-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
		{ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ,-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
		{ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ,-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
		{ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ,-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
		{ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ,-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
		{ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ,-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
		{ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ,-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
		{ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ,-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
		{ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ,-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
		{ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ,-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
		{ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ,-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
		{ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ,-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
		{ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ,-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
		{ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ,-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
		{ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ,-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
		{ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ,-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
		{ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ,-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
		{ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ,-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
		{ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ,-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
		{ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ,-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
		{ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ,-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
		{ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ,-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
		{ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ,-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
		{ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ,-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
		{ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ,-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
		{ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ,-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
		{ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ,-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
		{ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ,-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
		{ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ,-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 }
	};

	// used to show that it can always find a valid point between any two points on this grid with obstacles
	int proof_grid[8][8] = {
		{ -1, -1, -2, -1, -1, -1, -1, -2},
		{ -1, -1, -2, -1, -1, -1, -1, -1},
		{ -1, -1, -2, -1, -1, -1, -1, -1},
		{ -1, -1, -2, -1, -1, -1, -1, -1},
		{ -1, -2, -2, -1, -1, -1, -1, -1},
		{ -1, -1, -1, -1, -1, -1, -1, -1},
		{ -1, -2, -2, -1, -1, -2, -1, -2},
		{ -1, -1, -2, -1, -1, -1, -1, -1}
	};
	//
	/*std::cout << "Compare std::set to std::unordered::set" << std::endl;
	CompareSets(grid4by4, 20, 0, 0, 3, 3, "Grid4by4");
	CompareSets(grid8by4, 20, 0, 0, 7, 3, "Grid8by4");
	CompareSets(grid8by8, 20, 0, 0, 7, 7, "Grid8by8");
	CompareSets(grid16by16, 20, 0, 0, 15, 15, "Grid16by16");
	CompareSets(grid32by32, 20, 0, 0, 31, 31, "Grid32by32");

	AlgorithmComparision(grid4by4, 20, 0, 0, 3, 3, "Grid4by4");
	AlgorithmComparision(grid8by4, 20, 0, 0, 7, 3, "Grid8by4");
	AlgorithmComparision(grid8by8, 20, 0, 0, 7, 7, "Grid8by8");
	AlgorithmComparision(grid16by16, 20, 0, 0, 15, 15, "Grid16by16");
	AlgorithmComparision(grid32by32, 20, 0, 0, 31, 31, "Grid32by32");

	AlgorithmComparision(grid4by4, 20, 0, 0, 3, 3, "Grid4by4",true);
	AlgorithmComparision(grid8by4, 20, 0, 0, 7, 3, "Grid8by4", true);
	AlgorithmComparision(grid8by8, 20, 0, 0, 7, 7, "Grid8by8", true);
	AlgorithmComparision(grid16by16, 20, 0, 0, 15, 15, "Grid16by16", true);
	AlgorithmComparision(grid32by32, 20, 0, 0, 31, 31, "Grid32by32",true);*/

	std::cout << "Debug Test 0,0 to 0,7 visualisation:\nBest to put into fullscreen makes it easier to see f/g/h cost and visual of the grid.\nPress enter to cycle through results..." << std::endl;
	std::cin.ignore();

	DebugTestPath(proof_grid, 0, 0, 7, 7, AStarAlgorithm::kDiagonal, true);
	DebugTestPath(proof_grid, 0, 0, 7, 7, AStarAlgorithm::kEuclidean, true);
	DebugTestPath(proof_grid, 0, 0, 7, 7, AStarAlgorithm::kDijkstra, true);

	DebugTestPath(proof_grid, 0, 0, 7, 7, AStarAlgorithm::kManhattan);
	DebugTestPath(proof_grid, 0, 0, 7, 7, AStarAlgorithm::kDijkstra);

	std::cout << "Debug Test all nodes visualisation:\nBest to put into fullscreen makes it easier to see f/g/h cost and visual of the grid.\nPress enter to cycle through results..." << std::endl;
	std::cin.ignore();

	std::cout << ">>> With Diagonals <<<" << std::endl;
	std::cout << ">>> Heuristic: Diagonal <<<" << std::endl;
	DebugTestAllNodePaths(proof_grid, AStarAlgorithm::kDiagonal, true);
	std::cout << ">>> Heuristic: Euclidean <<<" << std::endl;
	DebugTestAllNodePaths(proof_grid, AStarAlgorithm::kEuclidean, true);
	std::cout << ">>> Heuristic: Dijkstra <<<" << std::endl;
	DebugTestAllNodePaths(proof_grid, AStarAlgorithm::kDijkstra, true);

	std::cin.ignore();

}